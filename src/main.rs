#![feature(plugin)]
#![plugin(rocket_codegen)]

extern crate rocket;
extern crate rocket_contrib;
#[macro_use]
extern crate serde_derive;

use rocket_contrib::Template;
use rocket::response::status::NotFound;
use rocket::response::NamedFile;
use std::path::Path;

#[derive(Serialize)]
struct TemplateContext {
    name: String,
    items: Vec<String>,
}

#[get("/create_new_model")]
fn index() -> Result<NamedFile, NotFound<String>> {
    let path = Path::new("templates/new_model.html.hbs");
    NamedFile::open(&path).map_err(|_| NotFound(format!("Cant find new_model")))
}

#[get("/hello/<name>")]
fn get(name: String) -> Template {
    let context = TemplateContext {
        name: name,
        items: ["one", "two", "three"]
            .iter()
            .map(|s| s.to_string())
            .collect(),
    };
    Template::render("index", &context)
}

fn main() {
    rocket::ignite()
        .mount("/", routes![get, index])
        .attach(Template::fairing())
        .launch();
}
